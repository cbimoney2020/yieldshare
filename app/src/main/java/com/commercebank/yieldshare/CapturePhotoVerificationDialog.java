package com.commercebank.yieldshare;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CapturePhotoVerificationDialog extends DialogFragment {
    @BindView(R.id.okay_button)
    View okayButton;

    private View.OnClickListener onClickListener;

    private static CapturePhotoVerificationDialog create(View.OnClickListener onClickListener) {
        CapturePhotoVerificationDialog dialog = new CapturePhotoVerificationDialog();
        dialog.onClickListener = onClickListener;
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_capture_identity, container, false);
        ButterKnife.bind(this, v);

        okayButton.setOnClickListener(v1 -> {
            if (null != onClickListener) {
                onClickListener.onClick(v1);
                dismiss();
            }
        });
        return v;
    }

    public static class Builder {
        private View.OnClickListener onClickListener;

        public CapturePhotoVerificationDialog build() {
            return CapturePhotoVerificationDialog.create(onClickListener);
        }

        public Builder setOnClickListener(View.OnClickListener onClickListener) {
            this.onClickListener = onClickListener;
            return this;
        }
    }
}
