package com.commercebank.yieldshare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VerifyDocumentationRequest {
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("imageData")
    @Expose
    private String imageData;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImageData() {
        return imageData;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }
}
