package com.commercebank.yieldshare.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Opportunity {
    @SerializedName("personName")
    @Expose
    private String personName;
    @SerializedName("opportunityName")
    @Expose
    private String opportunityName;
    @SerializedName("opportunityDesc")
    @Expose
    private String opportunityDescription;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("yieldAmount")
    @Expose
    private String yieldAmount;
    @SerializedName("drawableRes")
    @Expose
    private int drawableRes;

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getOpportunityName() {
        return opportunityName;
    }

    public void setOpportunityName(String opportunityName) {
        this.opportunityName = opportunityName;
    }

    public String getOpportunityDescription() {
        return opportunityDescription;
    }

    public void setOpportunityDescription(String opportunityDescription) {
        this.opportunityDescription = opportunityDescription;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getYieldAmount() {
        return yieldAmount;
    }

    public void setYieldAmount(String yieldAmount) {
        this.yieldAmount = yieldAmount;
    }

    public int getDrawableRes() {
        return drawableRes;
    }

    public void setDrawableRes(int drawableRes) {
        this.drawableRes = drawableRes;
    }
}
