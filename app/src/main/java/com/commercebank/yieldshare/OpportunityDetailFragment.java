package com.commercebank.yieldshare;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.commercebank.yieldshare.model.Opportunity;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class OpportunityDetailFragment extends DaggerFragment {
    @BindView(R.id.close_button)
    View closeButton;

    @BindView(R.id.opportunity_name)
    TextView opportunityName;
    @BindView(R.id.opportunity_desc)
    TextView opportunityDesc;
    @BindView(R.id.rating)
    TextView rating;
    @BindView(R.id.yield_amount)
    TextView yield;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_opportunity_detail, container, false);
        ButterKnife.bind(this, v);

        Bundle b = getArguments();
        if (null != b) {
            Opportunity o = new Gson().fromJson(b.getString("opportunity"), Opportunity.class);
            opportunityName.setText(o.getOpportunityName());
            opportunityDesc.setText(o.getOpportunityDescription());
            rating.setText(o.getRating());
            yield.setText(getString(R.string.yield_amount, o.getYieldAmount()));
        }

        closeButton.setOnClickListener(v1 -> {
            if (null != getActivity()) {
                getActivity().onBackPressed();
            }
        });

        return v;
    }
}
