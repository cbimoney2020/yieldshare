package com.commercebank.yieldshare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AccountSegmentActivity extends AppCompatActivity {
    @BindView(R.id.create_account)
    View createAccountButton;
    @BindView(R.id.sign_in)
    View signInButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_segment);
        ButterKnife.bind(this);

        createAccountButton.setOnClickListener(v -> startActivity(new Intent(this, CreateAccountActivity.class)));
        signInButton.setOnClickListener(v -> startActivity(new Intent(this, MainActivity.class)));
    }
}
