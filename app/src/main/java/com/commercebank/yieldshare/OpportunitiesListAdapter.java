package com.commercebank.yieldshare;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.commercebank.yieldshare.model.Opportunity;

import java.util.ArrayList;
import java.util.List;

public class OpportunitiesListAdapter extends RecyclerView.Adapter<OpportunitiesListAdapter.OpportunitiesViewHolder> {
    private List<Opportunity> opportunityList = new ArrayList<>();
    private OpportunitySelectedListener opportunitySelectedListener;

    @NonNull
    @Override
    public OpportunitiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.list_item_opportunity, null);
        return new OpportunitiesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OpportunitiesViewHolder holder, int position) {
        Opportunity o = opportunityList.get(position);

        holder.name.setText(o.getPersonName());
        holder.opportunityName.setText(o.getOpportunityName());
        holder.opportunityDesc.setText(o.getOpportunityDescription());
        holder.rating.setText(o.getRating());
        holder.profileImage.setImageBitmap(BitmapFactory.decodeResource(holder.itemView.getResources(), o.getDrawableRes()));
        holder.yieldAmount.setText(o.getYieldAmount());

        holder.itemView.setOnClickListener(v -> {
            if (null != opportunitySelectedListener) {
                opportunitySelectedListener.onOpportunitySelected(o);
            }
        });
    }

    @Override
    public int getItemCount() {
        return opportunityList.size();
    }

    void setList(List<Opportunity> opportunities) {
        opportunityList.clear();
        opportunityList.addAll(opportunities);
    }

    void setOpportunitySelectedListener(OpportunitySelectedListener opportunitySelectedListener) {
        this.opportunitySelectedListener = opportunitySelectedListener;
    }

    interface OpportunitySelectedListener {
        void onOpportunitySelected(Opportunity opportunity);
    }

    static class OpportunitiesViewHolder extends RecyclerView.ViewHolder {
        ImageView profileImage;
        TextView name;
        TextView opportunityName;
        TextView opportunityDesc;
        TextView rating;
        TextView yieldAmount;

        OpportunitiesViewHolder(View itemView) {
            super(itemView);

            profileImage = itemView.findViewById(R.id.profile_image);
            name = itemView.findViewById(R.id.name);
            opportunityName = itemView.findViewById(R.id.opportunity_name);
            opportunityDesc = itemView.findViewById(R.id.opportunity_desc);
            rating = itemView.findViewById(R.id.rating);
            yieldAmount = itemView.findViewById(R.id.yield_amount);
        }
    }
}
