package com.commercebank.yieldshare.dagger.modules;

import com.commercebank.yieldshare.CreateAccountActivity;
import com.commercebank.yieldshare.MainActivity;
import com.commercebank.yieldshare.OpportunityDetailFragment;
import com.commercebank.yieldshare.OpportunityMapFragment;
import com.commercebank.yieldshare.ProfileInformationActivity;
import com.commercebank.yieldshare.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {
    @ContributesAndroidInjector
    abstract SplashActivity splashActivity();

    @ContributesAndroidInjector
    abstract CreateAccountActivity createAccountActivity();
    @ContributesAndroidInjector
    abstract ProfileInformationActivity profileInformationActivity();

    @ContributesAndroidInjector
    abstract MainActivity mainActivity();
    @ContributesAndroidInjector
    abstract OpportunityMapFragment opportunityMapFragment();
    @ContributesAndroidInjector
    abstract OpportunityDetailFragment opportunityDetailFragment();
}
