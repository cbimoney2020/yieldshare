package com.commercebank.yieldshare.dagger.components;

import com.commercebank.yieldshare.YieldshareApplication;
import com.commercebank.yieldshare.dagger.modules.ActivityBuilder;
import com.commercebank.yieldshare.dagger.modules.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        ActivityBuilder.class,
        AppModule.class
})
public interface AppComponent extends AndroidInjector<YieldshareApplication> {
    void inject(YieldshareApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(YieldshareApplication application);

        AppComponent build();
    }
}
