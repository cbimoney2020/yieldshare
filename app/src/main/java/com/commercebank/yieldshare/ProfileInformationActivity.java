package com.commercebank.yieldshare;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;

import com.commercebank.yieldshare.model.VerifyDocumentationRequest;
import com.commercebank.yieldshare.services.YieldshareApi;
import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileInformationActivity extends DaggerAppCompatActivity {
    private static final int REQUEST_IMAGE_CAPTURE = 100;
    private static final int REQUEST_PERMISSIONS = 101;

    @Inject
    FirebaseAuth firebaseAuth;
    @Inject
    YieldshareApi yieldshareApi;

    @BindView(R.id.name)
    Button nameButton;
    @BindView(R.id.currency)
    Button currencyButton;
    @BindView(R.id.verify)
    Button verifyButton;
    @BindView(R.id.get_started_button)
    Button getStartedButton;
    @BindView(R.id.overlay_progress_bar)
    View overlayProgressBar;

    private File photoFile;
    private boolean verified;
    private String currency;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_information);
        ButterKnife.bind(this);

        if (null != firebaseAuth.getCurrentUser()) {
            nameButton.setText(firebaseAuth.getCurrentUser().getDisplayName());
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            currency = "rand";
            updateButtons();
        }

        currencyButton.setOnClickListener(v -> {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                currency = "rand";
                updateButtons();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS);
            }
        });

        verifyButton.setOnClickListener(v -> new CapturePhotoVerificationDialog.Builder()
                .setOnClickListener(v1 -> dispatchTakePictureIntent())
                .build()
            .show(getSupportFragmentManager(), "photo_dialog"));

        getStartedButton.setOnClickListener(v -> {
            Intent i = new Intent(ProfileInformationActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
            finish();
        });

        updateButtons();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            new SendPhotoTask().execute();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                currency = "rand";
                updateButtons();
            }
        }
    }

    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        try {
            return File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (Exception e) {
            return null;
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                photoFile = createImageFile();
            } catch (Exception ignore) {}

            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.commercebank.yieldshare.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private void updateButtons() {
        verifyButton.setText(verified ? R.string.identity_verified : R.string.verify_identity);
        verifyButton.setBackgroundResource(verified ? R.drawable.bg_verified_button : R.drawable.bg_verify_button);
        verifyButton.setTextColor(Color.parseColor(verified ? "#ff7ed321" : "#ff000000"));

        if (verified) {
            verifyButton.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        } else {
            verifyButton.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        }

        if (TextUtils.isEmpty(currency)) {
            currencyButton.setText(R.string.select_currency);
            currencyButton.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        } else {
            currencyButton.setText("Rand");
            currencyButton.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        }

        boolean completed = verified && !TextUtils.isEmpty(currency);
        getStartedButton.setAlpha(completed ? 1.0f : 0.4f);
        getStartedButton.setEnabled(completed);
    }

    private class SendPhotoTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            overlayProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            VerifyDocumentationRequest request = new VerifyDocumentationRequest();
            request.setUserId(firebaseAuth.getUid());

            try {
                byte[] photo = Files.readAllBytes(photoFile.toPath());
                request.setImageData(Base64.encodeToString(photo, Base64.NO_WRAP));
            } catch (IOException e) {
                return false;
            }

            Call<ResponseBody> call = yieldshareApi.verifyDocumentation(request);

            try {
                Response<ResponseBody> response = call.execute();
                return response.isSuccessful();
            } catch (IOException ignore) {}

            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            overlayProgressBar.setVisibility(View.GONE);

            verified = aBoolean;
            updateButtons();
        }
    }
}
