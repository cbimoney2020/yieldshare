package com.commercebank.yieldshare;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;

public class MainActivity extends DaggerAppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Inject
    FirebaseAuth firebaseAuth;

    View closeButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (null == firebaseAuth.getCurrentUser()) {
            finish();
            return;
        }

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (null != actionBar) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black);
            actionBar.setTitle(0);
        }

        View header = navigationView.getHeaderView(0);

        closeButton = header.findViewById(R.id.close_button);
        closeButton.setOnClickListener(v -> drawerLayout.closeDrawers());

        String displayName = firebaseAuth.getCurrentUser().getDisplayName();
        if (!TextUtils.isEmpty(displayName)) {
            String[] names = displayName.split(" ");
            if (names.length > 0) {
                displayName = names[0];
            }
        }

        TextView greeting = header.findViewById(R.id.greeting);
        greeting.setText(getString(R.string.greeting_text, displayName));

        navigationView.setNavigationItemSelectedListener(menuItem -> {
                    // set item as selected to persist highlight
                    menuItem.setChecked(true);
                    // close drawer when item is tapped
                    drawerLayout.closeDrawers();

                    switch (menuItem.getItemId()) {
                        case R.id.nav_sign_out:
                            firebaseAuth.signOut();

                            Intent i = new Intent(this, AccountSegmentActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                            finish();
                            break;
                    }

                    return true;
                });

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new OpportunityMapFragment())
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void setFullContentDisplay(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.full_content_frame, fragment, "details")
                .addToBackStack(null)
                .commit();
    }
}
