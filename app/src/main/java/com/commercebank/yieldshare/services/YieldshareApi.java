package com.commercebank.yieldshare.services;

import com.commercebank.yieldshare.model.VerifyDocumentationRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface YieldshareApi {
    @POST("/documentUpload")
    Call<ResponseBody> verifyDocumentation(@Body VerifyDocumentationRequest request);
}
