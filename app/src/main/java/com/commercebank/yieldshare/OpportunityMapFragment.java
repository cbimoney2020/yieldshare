package com.commercebank.yieldshare;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.commercebank.yieldshare.model.Opportunity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public class OpportunityMapFragment extends DaggerFragment implements OnMapReadyCallback {
    private GoogleMap map;

    @BindView(R.id.mv_main)
    MapView mapView;
    @BindView(R.id.opportunities)
    RecyclerView opportunitiesList;

    private BottomSheetBehavior bottomSheetBehavior;
    private OpportunitiesListAdapter opportunitiesListAdapter = new OpportunitiesListAdapter();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_opportunity_map, container, false);
        ButterKnife.bind(this, v);

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        bottomSheetBehavior = BottomSheetBehavior.from(opportunitiesList);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        List<Opportunity> opportunities = new ArrayList<>(2);
        Opportunity o1 = new Opportunity();
        o1.setPersonName("Kungawo Venter");
        o1.setOpportunityName("Cleaning");
        o1.setOpportunityDescription("I need some one to come wash the floors at my shop.  I have 3 rooms that need cleaning.");
        o1.setRating("5");
        o1.setYieldAmount("R144.45");
        o1.setDrawableRes(R.drawable.ic_profile_kungawo);
        opportunities.add(o1);
        Opportunity o2 = new Opportunity();
        o2.setPersonName("Thulebona Mhlanga");
        o2.setOpportunityName("Baby Sitting");
        o2.setOpportunityDescription("I need someone to watch a 4 year old I have running around my house.");
        o2.setRating("4.9");
        o2.setYieldAmount("R80.22");
        o2.setDrawableRes(R.drawable.ic_profile_thulebona);
        opportunities.add(o2);
        opportunitiesListAdapter.setList(opportunities);
        opportunitiesList.setAdapter(opportunitiesListAdapter);

        opportunitiesListAdapter.setOpportunitySelectedListener(opportunity -> {
            OpportunityDetailFragment f = new OpportunityDetailFragment();

            if (null != getFragmentManager()) {
                Bundle b = new Bundle();
                b.putString("opportunity", new Gson().toJson(opportunity));
                f.setArguments(b);
                ((MainActivity) getActivity()).setFullContentDisplay(f);
            }
        });

        setupRecyclerView();

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-29.7341639, 25.9391637f), 17.0f));
        map.getUiSettings().setMapToolbarEnabled(false);

        map.addMarker(new MarkerOptions()
                .position(new LatLng(-29.7323193, 25.939904))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));
        map.addMarker(new MarkerOptions()
                .position(new LatLng(-29.7339897f,25.938373f))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));

        map.setOnMarkerClickListener(marker -> {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            return false;
        });
    }

    private void setupRecyclerView() {
        opportunitiesList.setHasFixedSize(true);
        opportunitiesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        opportunitiesList.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        opportunitiesList.setItemAnimator(new DefaultItemAnimator());
    }
}
